# noise4grow

A noise generator for [Grow](https://gitlab.com/metagrowing/grow). This library provides functions for Perlin noise and Perlin turbulence.

Experimental code. Please do not use in production projects.

## Sytem requirements
- openjdk-11-jre
