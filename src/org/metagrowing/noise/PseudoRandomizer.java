/*
 * Copyright 2013, Thomas Jourdan
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package org.metagrowing.noise;

import java.util.Random;

public class PseudoRandomizer {
  Random random;
  long seed;

  public PseudoRandomizer(long seed) {
    this.seed = seed;
    this.random = new Random(seed);
  }

  public void setSeed(long seed) {
    this.seed = seed;
    this.random.setSeed(seed);
  }

  public int next(int n) {
    return this.random.nextInt(n);
  }

  /**
   * @return Returns the next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number generator's sequence.
   */
  public double uniform() {
    return this.random.nextDouble();
  }

  public double gaussian() {
    return this.random.nextGaussian();
  }

  @Override
  public String toString() {
    return "PseudoRandomizer [seed=" + this.seed + "]";
  }
  
}
