/*
 * Copyright (C) 2002 - 2005 Thomas Jourdan <develop@kandid.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.metagrowing.noise;

/**
 * @author thomas jourdan
 *
 */
public class Vector3 {
  public double x;
  public double y;
  public double z;
  public Vector3() {
    this.x = 0.0;
    this.y = 0.0;
    this.z = 0.0;
  }
  public Vector3(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}
