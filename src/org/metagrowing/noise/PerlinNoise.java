/*
 * Copyright 2013, Thomas Jourdan
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * This code is ported from an old Blender to Java.
 * The original version was writen in C.
 * The original version is licensed under GPL v2.
 * @see https://blender.org/
*/
package org.metagrowing.noise;


public class PerlinNoise {
  ImprovedNoise improvedPerlinNoise;

  public PerlinNoise(long seed) {
    this.improvedPerlinNoise = new ImprovedNoise();
    this.improvedPerlinNoise.seed(seed);
  }

  public void initializeNoise(long seed) {
    this.improvedPerlinNoise.seed(seed);
  }

  public double noise(double x, double y, double z) {
    return 0.5 * (1.0 + this.improvedPerlinNoise.noise(x, y, z));
  }

  /**
   * @see https://svn.blender.org/svnroot/bf-blender/trunk/blender/source/blender/python/mathutils/mathutils_noise.c
   * @param x
   * @param y
   * @param z
   * @param vnoise
   */
  public void vecNoise(double x, double y, double z, Vector3 vnoise) {
    /* Simply evaluate noise at 3 different positions */
    vnoise.x = this.noise(x + 9.321, y - 1.531, z - 7.951);
    vnoise.y = this.noise(x, y, z);
    vnoise.z = this.noise(x + 6.327, y + 0.1671, z - 2.672);
  }

  public double turbulence(double x, double y, double z, int octaves, double ampscale, double freqscale) {
    double amp = 1.0;
    double noise = this.noise(x, y, z);
    for (int i = 1; i < octaves; i++) {
      amp *= ampscale;
      x *= freqscale;
      y *= freqscale;
      z *= freqscale;
      double t = amp * (this.noise(x, y, z) - 0.5);
      noise += t;
    }
    return noise;
  }

  /**
   * @see https://svn.blender.org/svnroot/bf-blender/trunk/blender/source/blender/python/mathutils/mathutils_noise.c
   * @param x
   * @param y
   * @param z
   * @param octaves
   * @param ampscale
   * @param freqscale
   * @param vnoise
   */
  public void vecTurbulence(double x, double y, double z, int octaves, double ampscale, double freqscale, Vector3 vnoise) {
    vnoise.x = this.noise(x + 9.321, y - 1.531, z - 7.951);
    vnoise.y = this.noise(x, y, z);
    vnoise.z = this.noise(x + 6.327, y + 0.1671, z - 2.672);
    double amp = 1.0;
    for (int i = 1; i < octaves; i++) {
      amp *= ampscale;
      x *= freqscale;
      y *= freqscale;
      z *= freqscale;
      double nx = this.noise(x + 9.321, y - 1.531, z - 7.951);
      double ny = this.noise(x, y, z);
      double nz = this.noise(x + 6.327, y + 0.1671, z - 2.672);
      vnoise.x += amp * (nx - 0.5);
      vnoise.y += amp * (ny - 0.5);
      vnoise.z += amp * (nz - 0.5);
    }
    vnoise.x = (0.05 + vnoise.x / 1.1);
    vnoise.y = (0.05 + vnoise.y / 1.1);
    vnoise.z = (0.05 + vnoise.z / 1.1);
  }

}
