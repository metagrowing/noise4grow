/*
 * Copyright 2013, Thomas Jourdan
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package org.metagrowing.noisetest;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


import org.junit.Test;
import org.metagrowing.noise.ImprovedNoise;
import org.metagrowing.noise.PerlinNoise;
import org.metagrowing.noise.Vector3;

public class PerlinNoiseTest {
  
  interface SNoiseCalculation {
    double valueAt(double x, double y, double z);
    void checkRange(double value);
    void vectorAt(double x, double y, double z, Vector3 vnoise);
  }
  
  abstract class AbstractNoiseCalculation {
    PerlinNoise perlinNoise;
    
    public AbstractNoiseCalculation(PerlinNoise perlinNoise) {
      this.perlinNoise = perlinNoise;
    }
  }

  class RawNoiseCalculation extends AbstractNoiseCalculation implements SNoiseCalculation {
    ImprovedNoise improvedPerlinNoise;
    public RawNoiseCalculation(@SuppressWarnings("unused") PerlinNoise perlinNoise) {
      super(null);
      this.improvedPerlinNoise = new ImprovedNoise();
    }

    @Override
    public double valueAt(double x, double y, double z) {
      return this.improvedPerlinNoise.noise(x, y, z);
    }

    @Override
    public void vectorAt(double x, double y, double z, Vector3 vnoise) {
      // a vector is filled by calling the scalar noise function for every component of the vector 
    }

    @Override
    public void checkRange(double value) {
      assertTrue(value >= -1.0);
      assertTrue(value <=  1.0);
    }
  }

  class NoiseCalculation extends AbstractNoiseCalculation implements SNoiseCalculation {
    public NoiseCalculation(PerlinNoise perlinNoise) {
      super(perlinNoise);
    }

    @Override
    public double valueAt(double x, double y, double z) {
      return this.perlinNoise.noise(x, y, z);
    }

    @Override
    public void vectorAt(double x, double y, double z, Vector3 vnoise) {
      this.perlinNoise.vecNoise(x, y, z, vnoise);
    }

    @Override
    public void checkRange(double value) {
      assertTrue(value >= 0.0);
      assertTrue(value <=  1.0);
    }
  }

  class TurbulenceCalculation extends AbstractNoiseCalculation implements SNoiseCalculation {
    public TurbulenceCalculation(PerlinNoise perlinNoise) {
      super(perlinNoise);
    }

    @Override
    public double valueAt(double x, double y, double z) {
      return this.perlinNoise.turbulence(x, y, z, 6, 0.5, 2.0);
    }

    @Override
    public void vectorAt(double x, double y, double z, Vector3 vnoise) {
      this.perlinNoise.vecTurbulence(x, y, z, 6, 0.5, 2.0, vnoise);
    }

    @Override
    public void checkRange(double value) {
      assertTrue(value >= 0.0);
      assertTrue(value <=  1.0);
    }
  }

  protected void renderScalarImage(int width, int height, double z, SNoiseCalculation calculation) throws IOException {
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
    Graphics2D g2d = (Graphics2D) image.getGraphics();
    g2d.setColor(Color.BLACK);
    g2d.fillRect(0, 0, width, height);
    double min = Double.MAX_VALUE;
    double max = Double.MIN_VALUE;
    double dx = 1.0 / width;
    double dy = 1.0 / width;
    for(int ix=0; ix<width; ++ix) {
      double x = ix*dx;
      for(int iy=0; iy<height; ++iy) {
        double y = iy*dy;
        double scalar = calculation.valueAt(x, y, z);
        if(scalar < min) {
          min = scalar;
        }
        if(scalar > max) {
          max = scalar;
        }
        if(scalar < 0.0) {
          g2d.setColor(Color.BLUE);
        }
        else if(scalar > 1.0) {
          g2d.setColor(Color.RED);
        }
        else {
          float gray = (float)scalar;
          g2d.setColor(new Color(gray, gray, gray));
        }
        g2d.fillRect(ix, iy, 1, 1);
      }
    }
    String classname = calculation.getClass().getName();
    System.out.println(String.format("%+f ", min) + String.format("%+f ", max) + " " + classname);
    ImageIO.write(image, "png",new File("/dev/shm/scalar_noise_" + classname + "_z" + z + ".png"));
    calculation.checkRange(min);
    calculation.checkRange(max);
  }

  protected void renderVectorImage(int width, int height, double z, SNoiseCalculation calculation) throws IOException {
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
    Graphics2D g2d = (Graphics2D) image.getGraphics();
    g2d.setColor(Color.BLACK);
    g2d.fillRect(0, 0, width, height);
    Vector3 vnoise = new Vector3();
    double xmin = Double.MAX_VALUE;
    double xmax = Double.MIN_VALUE;
    double ymin = Double.MAX_VALUE;
    double ymax = Double.MIN_VALUE;
    double zmin = Double.MAX_VALUE;
    double zmax = Double.MIN_VALUE;
    double dx = 1.0 / width;
    double dy = 1.0 / width;
    for(int ix=0; ix<width; ++ix) {
      double x = ix*dx;
      for(int iy=0; iy<height; ++iy) {
        double y = iy*dy;
        calculation.vectorAt(x, y, z, vnoise);
        if(vnoise.x < xmin) {
          xmin = vnoise.x;
        }
        if(vnoise.x > xmax) {
          xmax = vnoise.x;
        }
        if(vnoise.y < ymin) {
          ymin = vnoise.y;
        }
        if(vnoise.y > ymax) {
          ymax = vnoise.y;
        }
        if(vnoise.z < zmin) {
          zmin = vnoise.z;
        }
        if(vnoise.z > zmax) {
          zmax = vnoise.z;
        }
        if(vnoise.x < 0.0 || vnoise.y < 0.0 || vnoise.z < 0.0) {
          g2d.setColor(Color.BLACK);
        }
        else if(vnoise.x > 1.0 || vnoise.y > 1.0 || vnoise.z > 1.0) {
          g2d.setColor(Color.WHITE);
        }
        else {
          g2d.setColor(new Color((float)vnoise.x, (float)vnoise.y, (float)vnoise.z));
        }
        g2d.fillRect(ix, iy, 1, 1);
      }
    }
    String classname = calculation.getClass().getName();
    System.out.println(String.format("%+f ", xmin) + String.format("%+f ", xmax) + String.format("%+f ", ymin) + String.format("%+f ", ymax) + String.format("%+f ", zmin) + String.format("%+f ", zmax) + " " + classname);
    ImageIO.write(image, "png",new File("/dev/shm/vector_noise_" + classname + "_z" + z + ".png"));

    calculation.checkRange(xmin);
    calculation.checkRange(xmax);
    calculation.checkRange(ymin);
    calculation.checkRange(ymax);
    calculation.checkRange(zmin);
    calculation.checkRange(zmax);
  }

  @Test
  public void testNoiseMargin() {
    ImprovedNoise improvedPerlinNoise = new ImprovedNoise();
    for(double x=0.0; x<=1.0001; x+=1.0) {
      for(double y=0.0; y<=1.0001; y+=1.0) {
        for(double z=0.0; z<=1.0001; z+=1.0) {
          double n = improvedPerlinNoise.noise(x, y, z);
          assertEquals(n, 0.0, 0.0);
          System.out.print(n);
          System.out.print(' ');
        }
        System.out.println();
      }
    }
  }

  @Test
  public void testNoise() throws IOException {
    double[] z1 = { 0.0, 0.1, 0.25, 0.5, 0.75, 0.9, 1.0, };
    scanRange(z1);
    double[] z2 = { -0.1, -0.25, -0.5, -0.75, -0.9, -1.0, };
    scanRange(z2);
    double[] z3 = { -1000.1, -100.25, -10.5, 10.75, 100.9, 1000.0, };
    scanRange(z3);
  }

  private void scanRange(double[] z) throws IOException {
    for(int iz=0; iz<z.length; ++iz) {
      renderScalarImage(256, 256, z[iz], new RawNoiseCalculation(null));
    }
    PerlinNoise perlinNoise = new PerlinNoise(1512);
    for(int iz=0; iz<z.length; ++iz) {
      renderScalarImage(256, 256, z[iz], new NoiseCalculation(perlinNoise));
    }
    for(int iz=0; iz<z.length; ++iz) {
      renderScalarImage(256, 256, z[iz], new TurbulenceCalculation(perlinNoise));
    }
    for(int iz=0; iz<z.length; ++iz) {
      renderVectorImage(256, 256, z[iz], new NoiseCalculation(perlinNoise));
    }
    for(int iz=0; iz<z.length; ++iz) {
      renderVectorImage(256, 256, z[iz], new TurbulenceCalculation(perlinNoise));
    }
  }

}
